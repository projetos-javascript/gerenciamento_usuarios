class UserController {

    constructor(formId, tableId) {

        this.formEl = document.getElementById(formId);
        this.tableEl = document.getElementById(tableId);        
    }

    // Methods

        // Adicionar Evento no Formulario
        formSubmit() {
            // Colhendo os dados do Formulario e Adicionando no OBJETO/JSON USER
            document.getElementById('form-user-create').addEventListener('submit', (event) => {
                
                event.preventDefault();        

                let btn =this.formEl.querySelector('[type=submit]');

                btn.disabled = true;

                let user = this.getValues();

                // MACETE: estamos fazendo isso poq mudou o tipo de dados, antes era objeto e agora é boolean
                // e estava tentando acessar a propriedade photo, sendo que agora é boolean
                if(!user) {
                    return false;
                }

                this.getPhoto().then(
                    (content) => {

                        user.photo = content;
                        this.addLine(user);

                        this.formEl.reset();
                        btn.disabled = false;
                        
                    }, (event) => {
                        console.error(event);
                    }
                );

            });
        }

        getPhoto() {

            return new Promise( (resolve, reject) => {
                let fileReader = new FileReader();
                // selecionando os inputs do formulario e deixando em array
                let elements = [...this.formEl.elements].filter(item => {
                    if (item.name === 'photo') {
                        // buscando o input com o name='photo'
                        return item;

                    }
                });            

                // Arquivo/CAMINHO DA RAIZ DA PASTA carregado 
                let file = elements[0].files[0];            

                // após a foto ser carregada executa este callback
                fileReader.onload = () => {
                    // aqui é o resultado e convertido em base64 e o img=src entende isso
                    resolve(fileReader.result);                
                };  

                // FileReader já tem o método/propriedade de error
                fileReader.onerror = (event) => {
                    reject(event);
                }

                if (file) {    
                    fileReader.readAsDataURL(file);
                } else {
                    resolve('dist/img/boxed-bg.jpg');
                }
            });
        }        

        // Coletando dados do FORM
        getValues() {

            let user = {};  // JSON Vázio      
            let isValid = true;

            // transformando em Array para usar o forEach
            [...this.formEl.elements].forEach( (field) => {

                // Validando os campos se foram preenchidos
                if (['name', 'email', 'password'].indexOf(field.name) > -1 && !field.value ) {
                    field.parentElement.classList.add('has-error');
                    isValid = false;
                }

                if ( field.name == "gender") {         
                    if (field.checked) {
                        user[field.name] = field.value;
                    }
                } else if ( field.name == "admin"){
                    user[field.name] = field.checked;
                } else {
                    user[field.name] = field.value;
                }

            });

            if(!isValid) {
                // Vai retornar FALSE e vai para o estado inicial que é o IF de CIMA dos CAMPOS que solicitamos
                // com isso, ativa a class dos campos e trava aqui ate ficar TRUE
                return false;
            }

            // Instanciando objeto do USER
            const objectUser = new User(
                user.name, 
                user.gender, 
                user.birth, 
                user.country, 
                user.email,
                user.password, 
                user.photo, 
                user.admin,
            );

            return objectUser;
            
        }

        // Adicionar dados na TABELA - Esse é o ultimo passo
        // esse parametro dataUser é os dados que o getValues manda na variavel USER do formSubmit
        addLine(dataUser) {

            let tr = document.createElement('tr');
            // esse USER é uma variavel, podemos colocar qualquer nome
            tr.dataset.user = JSON.stringify(dataUser);            

            tr.innerHTML = 
            `
                <td><img src="${dataUser.photo}" alt="User Image" class="img-circle img-sm"></td>
                <td> ${dataUser.name} </td>
                <td> ${dataUser.email} </td>
                <td> ${ (dataUser.admin) ? 'Sim' : 'Não'} </td>
                <td> ${ Utils.dateFormat(dataUser.register) }</td>
                <td>
                    <button type="button" class="btn btn-primary btn-xs btn-flat">Editar</button>
                    <button type="button" class="btn btn-danger btn-xs btn-flat">Excluir</button>
                </td>
            `;

            // adicionando elemento na tabela
            this.tableEl.appendChild(tr);

            // vai verificar a quantidade de usuarios cadastrados
            this.updateCount();

        }        


        updateCount() {       

            let numberUsers = 0;
            let numberAdmin = 0;

            // forEach de Usuarios
            [...this.tableEl.children].forEach( (tr) => {
            //  Estamos somando para poder proseeguir o loop de repetição
                numberUsers++;

                // console.dir('teste' + tr.dataset.users);
                
                // let user = JSON.parse(tr.dataset.user);

                // if (user._admin) numberAdmin++;    
                
            });

            document.querySelector("#number-users").innerHTML = numberUsers;
            document.querySelector("#number-users-admin").innerHTML = numberAdmin;

        }

}
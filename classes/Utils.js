class Utils {

   static dateFormat(date) {
// O parametro date esta representando a instancia do New Date() e com isso, estamo usando os métodos da Data
// Este método está sendo chamado no arquivo do UserController
        return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()+' '+date.getHours()+':'+date.getMinutes();

    }

}
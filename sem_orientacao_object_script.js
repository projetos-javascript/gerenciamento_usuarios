
let fields = document.querySelectorAll("#form-user-create [name]");
let user = {};


// Adicionando usuario na tabela
function addLine(dataUser) {

    console.log(dataUser);

    document.getElementById('table-users').innerHTML = 
    `
    <tr>
            <td><img src="dist/img/user1-128x128.jpg" alt="User Image" class="img-circle img-sm"></td>
            <td>${dataUser.name}</td>
            <td>${dataUser.email}</td>
            <td>${dataUser.admin}</td>
            <td>02/04/2018</td>
            <td>
                <button type="button" class="btn btn-primary btn-xs btn-flat">Editar</button>
                <button type="button" class="btn btn-danger btn-xs btn-flat">Excluir</button>
            </td>
    </tr>
     `;
}


// console.log(user);

// Colhendo os dados do Formulario e Adicionando no OBJETO/JSON USER
document.getElementById('form-user-create').addEventListener('submit', (event) => {
    
    event.preventDefault();

    fields.forEach( (field) => {
   
        if ( field.name == "gender") {         
            if (field.checked) {
                user[field.name] = field.value;
            }
        } else {
            user[field.name] = field.value;
        } 
    });

    const objectUser = new User(
        user.name, 
        user.gender, 
        user.birth, 
        user.country, 
        user.password, 
        user.photo, 
        user.admin,
    );

    addLine(objectUser);

});

// document.querySelectorAll('button').forEach(function(){
//     this.addEventListener('click', () => {
//         console.log('click');        
//     })
// });

